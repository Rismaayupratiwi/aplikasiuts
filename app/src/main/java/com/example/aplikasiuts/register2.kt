package com.example.aplikasiuts

import android.app.ProgressDialog
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_register.*

class register2 : AppCompatActivity(), View.OnClickListener {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_register)
    register.setOnClickListener(this)
    override fun onClick(v: View?) {
        var email = email2.text.toString()
        var password = password2.text.toString()
        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(
                this,
                "Username and password can't be empty ", Toast.LENGTH_SHORT
            ).show()
        } else {
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Succesfully Register", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_SHORT).show()
                }
        }

    }
}

