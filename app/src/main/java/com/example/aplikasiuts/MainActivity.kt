package com.example.aplikasiuts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    var fbaut = FirebaseAuth.getInstance()
    lateinit var viewDial: View
    val Tag = "openfcm"
    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loc.setOnClickListener(this)
        ins.setOnClickListener(this)
        logout.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ins->{

            }
            R.id.loc->{
                val intent = Intent(this, maps::class.java)
                startActivity(intent)
            }
            R.id.logout->{
                out("Ingin Keluar Dari Aplikasi !!!")
            }
        }
    }
    fun out(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
                fbaut.signOut()
                finish()
            }
            .setCancelClickListener{ sDialog -> sDialog.dismissWithAnimation()
            }
            .show()
    }
}



